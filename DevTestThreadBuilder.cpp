///-----------------------------------------------------------------
///
/// @file      DevTestThreadBuilder.cpp
/// @author    Ken
/// Created:   5/03/2013 9:48:37 AM
/// @section   DESCRIPTION
///            DevTestThreadBuilder class implementation
///
///------------------------------------------------------------------

#include "DevTestThreadBuilder.h"
#include "Objects/MingW/DevTestThreadBuilder_private.h"
#include "CheckForUpdatesThread.h"
#include <wx/aboutdlg.h>
#include <wx/html/htmlwin.h>
#include <wx/statline.h>
#include <wx/checkbox.h>
#include <wx/clipbrd.h>
#include <wx/datetime.h>
#include <wx/valtext.h>
#include <wx/valnum.h>

//#include "DropFiles.h"	// Will be used for drag/drop MD5 calculation

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// DevTestThreadBuilder
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(DevTestThreadBuilder,wxFrame)
	////Manual Code Start
	////Manual Code End

	EVT_CLOSE(DevTestThreadBuilder::OnClose)
	EVT_MENU(ID_MNU_EXIT_1102, DevTestThreadBuilder::Mnuexit1102Click)
	EVT_MENU(ID_MNU_UPDATE_1106, DevTestThreadBuilder::Mnuupdate1106Click)
	EVT_MENU(ID_MNU_WEBSITE_1104, DevTestThreadBuilder::Mnuwebsite1104Click)
	EVT_MENU(ID_MNU_ABOUT_1105, DevTestThreadBuilder::Mnuabout1105Click)
	EVT_TEXT(ID_WXEDITAPPNAME, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_TEXT(ID_WXEDITHOMEPAGE, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_TEXT(ID_WXEDITAPPVERSION, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_SPINCTRL(ID_WXSPINCTRL1, DevTestThreadBuilder::OnSpinCtrlUpdate)
	EVT_CHOICE(ID_WXCHOICELICENCE, DevTestThreadBuilder::WxChoice1Selected)
	EVT_CHOICE(ID_WXCHOICEPERMISSIONTYPE, DevTestThreadBuilder::WxChoicePermissionTypeChanged)
	EVT_TEXT(ID_WXEDITPERMISSIONLINK, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_TEXT(ID_WXEDITPERMISSIONTEXT, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_CHOICE(ID_WXCHOICECATEGORY, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_TEXT(ID_WXEDITSOURCECODELINK, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_TEXT(ID_WXEDITDOWNLOADLINK, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_TEXT(ID_WXEDITDOWNLOADSIZE, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_TEXT(ID_WXEDITINSTALLSIZE, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_CHOICE(ID_WXCHOICEINSTALLTYPE, DevTestThreadBuilder::WxChoiceInstallTypeChanged)
	EVT_TEXT(ID_WXEDITADDITIONALSIZE, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_TEXT(ID_WXEDITMD5, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_TEXT(ID_WXEDITDESCRIPTION, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_CHECKBOX(ID_WXCHECKBOXRELEASENOTES, DevTestThreadBuilder::OnCtrlUpdate)
	EVT_BUTTON(ID_WXBUTTONCOPYTITLE, DevTestThreadBuilder::OnCopyTitleClick)
	EVT_BUTTON(ID_WXBUTTONCOPYBODY, DevTestThreadBuilder::OnCopyBodyClick)
	EVT_COMMAND(wxID_ANY, wxEVT_CHECKFORUPDATESTHREAD, DevTestThreadBuilder::OnCheckUpdateThread)
END_EVENT_TABLE()
////Event Table End

DevTestThreadBuilder::DevTestThreadBuilder(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxFrame(parent, id, title, position, size, style)
{
//	fDownloadSize = 0;
//	fInstallSize = 0;
//	fAdditionalSize = 0;

	CreateGUIControls();

	WxBoxSizerBase->SetSizeHints(this);
	Center();
	WxPanel22->Hide();

	UpdateOutput();

	bIsLookupRunning = false;
	bIsCheckingUpdates = false;
	CheckForUpdates(true);
}

DevTestThreadBuilder::~DevTestThreadBuilder()
{
}

void DevTestThreadBuilder::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start



	WxPanelBase = new wxPanel(this, ID_WXPANELBASE, wxPoint(0, 0), wxSize(465, 547));

	WxBoxSizerBase = new wxBoxSizer(wxHORIZONTAL);
	WxPanelBase->SetSizer(WxBoxSizerBase);
	WxPanelBase->SetAutoLayout(true);

	WxPanel1 = new wxPanel(WxPanelBase, ID_WXPANEL1, wxPoint(0, 0), wxDefaultSize/*wxSize(400, 547)*/);
	WxBoxSizerBase->Add(WxPanel1, 0, wxALIGN_LEFT | wxALL, 10);

	WxBoxSizer1 = new wxBoxSizer(wxVERTICAL);
	WxPanel1->SetSizer(WxBoxSizer1);
	WxPanel1->SetAutoLayout(true);

	WxPanel2 = new wxPanel(WxPanel1, ID_WXPANEL2, wxPoint(0, 0), wxDefaultSize/*wxSize(390, 69)*/);
	WxBoxSizer1->Add(WxPanel2, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 0);

	WxBoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel2->SetSizer(WxBoxSizer2);
	WxPanel2->SetAutoLayout(true);

	WxPanel9 = new wxPanel(WxPanel2, ID_WXPANEL9, wxPoint(5, 5), wxDefaultSize/*wxSize(196, 69)*/);
	WxBoxSizer2->Add(WxPanel9, 1, wxALIGN_CENTER | wxALL |wxEXPAND, 5);

	WxBoxSizer9 = new wxBoxSizer(wxVERTICAL);
	WxPanel9->SetSizer(WxBoxSizer9);
	WxPanel9->SetAutoLayout(true);

	WxStaticText1 = new wxStaticText(WxPanel9, ID_WXSTATICTEXT1, _("App Name"), wxPoint(5, 5), wxDefaultSize, wxALIGN_LEFT, _("WxStaticText1"));
	WxStaticText1->SetToolTip(_("The name of the base application\neg. Development Test Thread Builder"));
	WxBoxSizer9->Add(WxStaticText1, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	WxEditAppName = new wxTextCtrl(WxPanel9, ID_WXEDITAPPNAME, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(200, 26)*/, wxEXPAND, wxDefaultValidator, _("WxEditAppName"));
	WxBoxSizer9->Add(WxEditAppName, 1, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM | wxEXPAND, 5);

	WxPanel10 = new wxPanel(WxPanel2, ID_WXPANEL10, wxPoint(211, 5), wxDefaultSize/*wxSize(231, 69)*/);
	WxBoxSizer2->Add(WxPanel10, 1, wxALIGN_CENTER | wxALL |wxEXPAND, 5);

	WxBoxSizer10 = new wxBoxSizer(wxVERTICAL);
	WxPanel10->SetSizer(WxBoxSizer10);
	WxPanel10->SetAutoLayout(true);

	WxStaticText6 = new wxStaticText(WxPanel10, ID_WXSTATICTEXT1, _("Homepage Link"), wxPoint(5, 5), wxDefaultSize, wxALIGN_LEFT, _("WxStaticText6"));
	WxStaticText6->SetToolTip(_("The homepage of the base application"));
	WxBoxSizer10->Add(WxStaticText6, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	WxEditHomepage = new wxTextCtrl(WxPanel10, ID_WXEDITHOMEPAGE, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(219, 26)*/, wxEXPAND, wxDefaultValidator, _("WxEditHomepage"));
	WxBoxSizer10->Add(WxEditHomepage, 1, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM | wxEXPAND, 5);

	WxPanel3 = new wxPanel(WxPanel1, ID_WXPANEL3, wxPoint(24, 69), wxDefaultSize/*wxSize(401, 69)*/);
	WxBoxSizer1->Add(WxPanel3, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 0);

	WxBoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel3->SetSizer(WxBoxSizer3);
	WxPanel3->SetAutoLayout(true);

	WxPanel4 = new wxPanel(WxPanel3, ID_WXPANEL4, wxPoint(5, 5), wxDefaultSize/*wxSize(127, 69)*/);
	WxBoxSizer3->Add(WxPanel4, 0, wxALIGN_LEFT | wxALL, 5);

	WxBoxSizer4 = new wxBoxSizer(wxVERTICAL);
	WxPanel4->SetSizer(WxBoxSizer4);
	WxPanel4->SetAutoLayout(true);

	WxStaticText2 = new wxStaticText(WxPanel4, ID_WXSTATICTEXT2, _("App Version"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText2"));
	WxStaticText2->SetToolTip(_("The version of the base application\neg. 1.0.0"));
	WxBoxSizer4->Add(WxStaticText2, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	WxEditAppVersion = new wxTextCtrl(WxPanel4, ID_WXEDITAPPVERSION, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(115, 26)*/, 0, wxDefaultValidator, _("WxEditAppVersion"));
	WxBoxSizer4->Add(WxEditAppVersion, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanel5 = new wxPanel(WxPanel3, ID_WXPANEL5, wxPoint(142, 5), wxDefaultSize/*wxSize(81, 69)*/);
	WxBoxSizer3->Add(WxPanel5, 0, wxALIGN_LEFT | wxALL |wxEXPAND, 5);

	WxBoxSizer5 = new wxBoxSizer(wxVERTICAL);
	WxPanel5->SetSizer(WxBoxSizer5);
	WxPanel5->SetAutoLayout(true);

	WxStaticText3 = new wxStaticText(WxPanel5, ID_WXSTATICTEXT3, _("DT Version"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText3"));
	WxStaticText3->SetToolTip(_("The development test version\neg. 2"));
	WxBoxSizer5->Add(WxStaticText3, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	WxSpinCtrl1 = new wxSpinCtrl(WxPanel5, ID_WXSPINCTRL1, _("1"), wxPoint(5, 24), wxDefaultSize/*wxSize(69, 24)*/, wxSP_ARROW_KEYS, 1, 100, 1);
	WxBoxSizer5->Add(WxSpinCtrl1, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanel11 = new wxPanel(WxPanel3, ID_WXPANEL11, wxPoint(233, 5), wxDefaultSize/*wxSize(126, 69)*/);
	WxBoxSizer3->Add(WxPanel11, 0, wxALIGN_LEFT | wxALL, 5);

	WxBoxSizer11 = new wxBoxSizer(wxVERTICAL);
	WxPanel11->SetSizer(WxBoxSizer11);
	WxPanel11->SetAutoLayout(true);

	WxStaticText7 = new wxStaticText(WxPanel11, ID_WXSTATICTEXT7, _("Category"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText7"));
	WxStaticText7->SetToolTip(_("The app category"));
	WxBoxSizer11->Add(WxStaticText7, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	wxArrayString arrayStringFor_WxChoiceCategory;
	arrayStringFor_WxChoiceCategory.Add(_("Accessibility"));
	arrayStringFor_WxChoiceCategory.Add(_("Development"));
	arrayStringFor_WxChoiceCategory.Add(_("Education"));
	arrayStringFor_WxChoiceCategory.Add(_("Games"));
	arrayStringFor_WxChoiceCategory.Add(_("Graphics & Pictures"));
	arrayStringFor_WxChoiceCategory.Add(_("Internet"));
	arrayStringFor_WxChoiceCategory.Add(_("Music & Video"));
	arrayStringFor_WxChoiceCategory.Add(_("Office"));
	arrayStringFor_WxChoiceCategory.Add(_("Security"));
	arrayStringFor_WxChoiceCategory.Add(_("Utilities"));
	WxChoiceCategory = new wxChoice(WxPanel11, ID_WXCHOICECATEGORY, wxPoint(5, 24), wxDefaultSize/*wxSize(115, 23)*/, arrayStringFor_WxChoiceCategory, 0, wxDefaultValidator, _("WxChoiceCategory"));
	WxChoiceCategory->SetSelection(9);
	WxBoxSizer11->Add(WxChoiceCategory, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanel12 = new wxPanel(WxPanel1, ID_WXPANEL12, wxPoint(6, 208), wxDefaultSize/*wxSize(438, 69)*/);
	WxBoxSizer1->Add(WxPanel12, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 0);

	WxBoxSizer12 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel12->SetSizer(WxBoxSizer12);
	WxPanel12->SetAutoLayout(true);

	WxPanel14 = new wxPanel(WxPanel12, ID_WXPANEL14, wxPoint(5, 5), wxDefaultSize/*wxSize(196, 69)*/);
	WxBoxSizer12->Add(WxPanel14, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 5);

	WxBoxSizer14 = new wxBoxSizer(wxVERTICAL);
	WxPanel14->SetSizer(WxBoxSizer14);
	WxPanel14->SetAutoLayout(true);

	WxStaticText8 = new wxStaticText(WxPanel14, ID_WXSTATICTEXT8, _("Download URL"), wxPoint(55, 5), wxDefaultSize, 0, _("WxStaticText8"));
	WxStaticText8->SetToolTip(_("The link to the app installer"));
	WxBoxSizer14->Add(WxStaticText8, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP | wxEXPAND, 5);

	WxEditDownloadLink = new wxTextCtrl(WxPanel14, ID_WXEDITDOWNLOADLINK, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(184, 26)*/, wxEXPAND, wxDefaultValidator, _("WxEditDownloadLink"));
	WxBoxSizer14->Add(WxEditDownloadLink, 1, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM | wxEXPAND, 5);

	WxPanel15 = new wxPanel(WxPanel12, ID_WXPANEL15, wxPoint(211, 5), wxDefaultSize/*wxSize(93, 69)*/);
	WxBoxSizer12->Add(WxPanel15, 0, wxALIGN_LEFT | wxALL, 5);

	WxBoxSizer15 = new wxBoxSizer(wxVERTICAL);
	WxPanel15->SetSizer(WxBoxSizer15);
	WxPanel15->SetAutoLayout(true);

	WxStaticText10 = new wxStaticText(WxPanel15, ID_WXSTATICTEXT10, _("D/L Size (MB)"), wxPoint(8, 5), wxDefaultSize, 0, _("WxStaticText10"));
	WxStaticText10->SetToolTip(_("The download size of the installer"));
	WxBoxSizer15->Add(WxStaticText10, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

//	wxFloatingPointValidator<float>
//		downloadSizeValidator(2, &fDownloadSize, wxNUM_VAL_ZERO_AS_BLANK | wxNUM_VAL_NO_TRAILING_ZEROES);
	wxFloatingPointValidator<float>
		downloadSizeValidator(2, NULL, wxNUM_VAL_ZERO_AS_BLANK | wxNUM_VAL_NO_TRAILING_ZEROES);
	downloadSizeValidator.SetRange(0, 1000);

	WxEditDownloadSize = new wxTextCtrl(WxPanel15, ID_WXEDITDOWNLOADSIZE, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(81, 26)*/, 0, downloadSizeValidator, _("WxEditDownloadSize"));
	WxBoxSizer15->Add(WxEditDownloadSize, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanel16 = new wxPanel(WxPanel12, ID_WXPANEL16, wxPoint(314, 5), wxDefaultSize/*wxSize(110, 69)*/);
	WxBoxSizer12->Add(WxPanel16, 0, wxALIGN_LEFT | wxALL, 5);

	WxBoxSizer16 = new wxBoxSizer(wxVERTICAL);
	WxPanel16->SetSizer(WxBoxSizer16);
	WxPanel16->SetAutoLayout(true);

	WxStaticText11 = new wxStaticText(WxPanel16, ID_WXSTATICTEXT11, _("Install Size (MB)"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText11"));
	WxStaticText11->SetToolTip(_("The installation size of the app"));
	WxBoxSizer16->Add(WxStaticText11, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	wxFloatingPointValidator<float>
		installSizeValidator(2, NULL, wxNUM_VAL_ZERO_AS_BLANK | wxNUM_VAL_NO_TRAILING_ZEROES);
	installSizeValidator.SetRange(0, 1000);

	WxEditInstallSize = new wxTextCtrl(WxPanel16, ID_WXEDITINSTALLSIZE, _(""), wxPoint(8, 24), wxDefaultSize/*wxSize(80, 26)*/, 0, installSizeValidator, _("WxEditInstallSize"));
	WxBoxSizer16->Add(WxEditInstallSize, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanel20 = new wxPanel(WxPanel1, ID_WXPANEL20, wxDefaultPosition/*wxPoint(6, 208)*/, wxDefaultSize);
	WxBoxSizer1->Add(WxPanel20, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 0);

	WxBoxSizer20 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel20->SetSizer(WxBoxSizer20);
	WxPanel20->SetAutoLayout(true);

//WxPanel20->SetBackgroundColour(wxColour(255, 0, 0, 255));
	WxPanel21 = new wxPanel(WxPanel20, ID_WXPANEL21, wxPoint(5, 5), wxDefaultSize);
	WxBoxSizer20->Add(WxPanel21, 0, wxALIGN_LEFT | wxALL | wxEXPAND, 5);

	WxBoxSizer21 = new wxBoxSizer(wxVERTICAL);
	WxPanel21->SetSizer(WxBoxSizer21);
	WxPanel21->SetAutoLayout(true);

//WxPanel21->SetBackgroundColour(wxColour(0, 255, 0, 255));

	WxStaticText14 = new wxStaticText(WxPanel21, ID_WXSTATICTEXT14, _("Install Type"), wxPoint(55, 5), wxDefaultSize, 0, _("WxStaticText14"));
	WxStaticText14->SetToolTip(_("The type of installer"));
	WxBoxSizer21->Add(WxStaticText14, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	wxArrayString arrayStringFor_WxChoiceInstallType;
	arrayStringFor_WxChoiceInstallType.Add(_("Offline"));
	arrayStringFor_WxChoiceInstallType.Add(_("Online"));
	WxChoiceInstallType = new wxChoice(WxPanel21, ID_WXCHOICEINSTALLTYPE, wxPoint(5, 24), wxDefaultSize/*wxSize(115, 23)*/, arrayStringFor_WxChoiceInstallType, 0, wxDefaultValidator, _("WxChoiceCategory"));
	WxChoiceInstallType->SetSelection(0);
	WxBoxSizer21->Add(WxChoiceInstallType, 1, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanel22 = new wxPanel(WxPanel20, ID_WXPANEL22, wxPoint(5, 5), wxDefaultSize);
	WxBoxSizer20->Add(WxPanel22, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 5);

	WxBoxSizer22 = new wxBoxSizer(wxVERTICAL);
	WxPanel22->SetSizer(WxBoxSizer22);
	WxPanel22->SetAutoLayout(true);
//WxPanel22->SetBackgroundColour(wxColour(0, 0, 255, 255));

	WxStaticText15 = new wxStaticText(WxPanel22, ID_WXSTATICTEXT15, _("Online D/L Size (MB)"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText15"));
	WxStaticText15->SetToolTip(_("(optional) The size of data downloaded by the online installer"));
	WxBoxSizer22->Add(WxStaticText15, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	wxFloatingPointValidator<float>
		additionalSizeValidator(2, NULL, wxNUM_VAL_ZERO_AS_BLANK | wxNUM_VAL_NO_TRAILING_ZEROES);
	additionalSizeValidator.SetRange(0, 1000);

	WxEditAdditionalSize = new wxTextCtrl(WxPanel22, ID_WXEDITADDITIONALSIZE, _(""), wxPoint(8, 24), wxDefaultSize/*wxSize(80, 26)*/, 0, additionalSizeValidator, _("WxEditInstallSize"));
	WxBoxSizer22->Add(WxEditAdditionalSize, 1, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanel6 = new wxPanel(WxPanel1, ID_WXPANEL6, wxPoint(33, 138), wxDefaultSize/*wxSize(384, 40)*/, wxEXPAND);
	WxBoxSizer1->Add(WxPanel6, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 0);
//WxPanel6->SetBackgroundColour(wxColour(255,0,0,255));
	WxBoxSizer6 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel6->SetSizer(WxBoxSizer6);
	WxPanel6->SetAutoLayout(true);

	WxPanel7 = new wxPanel(WxPanel6, ID_WXPANEL7, wxPoint(5, 5), wxDefaultSize/*wxSize(87, 49)*/);
	WxBoxSizer6->Add(WxPanel7, 0, wxALIGN_LEFT | wxALL, 5);

	WxBoxSizer7 = new wxBoxSizer(wxVERTICAL);
	WxPanel7->SetSizer(WxBoxSizer7);
	WxPanel7->SetAutoLayout(true);

	WxStaticText4 = new wxStaticText(WxPanel7, ID_WXSTATICTEXT4, _("Licence"), wxPoint(5, 5), wxDefaultSize, 0, _("WxStaticText4"));
	WxStaticText4->SetToolTip(_("The licence of the base app"));
	WxBoxSizer7->Add(WxStaticText4, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);
//WxPanel7->SetBackgroundColour(wxColour(0,0,255,255));

	wxArrayString arrayStringFor_WxChoiceLicence;
	arrayStringFor_WxChoiceLicence.Add(_("Open Source"));
	arrayStringFor_WxChoiceLicence.Add(_("Freeware"));
	WxChoiceLicence = new wxChoice(WxPanel7, ID_WXCHOICELICENCE, wxPoint(5, 24), wxDefaultSize/*wxSize(115, 23)*/, arrayStringFor_WxChoiceLicence, 0, wxDefaultValidator, _("WxChoiceLicence"));
	WxChoiceLicence->SetSelection(0);
	WxBoxSizer7->Add(WxChoiceLicence, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanel8 = new wxPanel(WxPanel6, ID_WXPANEL8, wxPoint(142, 5), wxDefaultSize/*wxSize(251, 49)*/);
	WxBoxSizer6->Add(WxPanel8, 1, wxALIGN_LEFT | wxALL |wxEXPAND, 5);
//WxPanel8->SetBackgroundColour(wxColour(0,255,0,255));

	WxBoxSizer8 = new wxBoxSizer(wxVERTICAL);
	WxPanel8->SetSizer(WxBoxSizer8);
	WxPanel8->SetAutoLayout(true);

	WxStaticText5 = new wxStaticText(WxPanel8, ID_WXSTATICTEXT5, _("Source Code URL"), wxPoint(67, 5), wxDefaultSize, 0, _("WxStaticText5"));
	WxStaticText5->SetToolTip(_("(optional) A link to the source code of the base app"));
	WxBoxSizer8->Add(WxStaticText5, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	WxEditSourceCodeLink = new wxTextCtrl(WxPanel8, ID_WXEDITSOURCECODELINK, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(219, 26)*/, wxEXPAND, wxDefaultValidator, _("WxEditSourceCodeLink"));
	WxBoxSizer8->Add(WxEditSourceCodeLink, 1, wxALIGN_LEFT | wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM | wxEXPAND, 5);

	WxPanel23 = new wxPanel(WxPanel6, ID_WXPANEL23, wxPoint(142, 5), wxDefaultSize/*wxSize(251, 49)*/);
	WxBoxSizer6->Add(WxPanel23, 1, wxALIGN_LEFT | wxALL |wxEXPAND, 0);

	WxBoxSizer23 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel23->SetSizer(WxBoxSizer23);
	WxPanel23->SetAutoLayout(true);

	WxPanel24 = new wxPanel(WxPanel23, ID_WXPANEL24, wxPoint(142, 5), wxDefaultSize/*wxSize(251, 49)*/);
	WxBoxSizer23->Add(WxPanel24, 1, wxALIGN_LEFT | wxALL |wxEXPAND, 5);

	WxBoxSizer24 = new wxBoxSizer(wxVERTICAL);
	WxPanel24->SetSizer(WxBoxSizer24);
	WxPanel24->SetAutoLayout(true);

	WxStaticText17 = new wxStaticText(WxPanel24, ID_WXSTATICTEXT17, _("Proof Type"), wxPoint(67, 5), wxDefaultSize, 0, _("WxStaticText17"));
	WxStaticText17->SetToolTip(_("The type of proof you have of permission to bundle freeware apps"));
	WxBoxSizer24->Add(WxStaticText17, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	wxArrayString arrayStringFor_WxChoicePermissionType;
	arrayStringFor_WxChoicePermissionType.Add(_("Link"));
	arrayStringFor_WxChoicePermissionType.Add(_("Text"));
	WxChoicePermissionType = new wxChoice(WxPanel24, ID_WXCHOICEPERMISSIONTYPE, wxPoint(5, 24), wxDefaultSize/*wxSize(115, 23)*/, arrayStringFor_WxChoicePermissionType, 0, wxDefaultValidator, _("WxChoiceLicence"));
	WxChoicePermissionType->SetSelection(0);
	WxBoxSizer24->Add(WxChoicePermissionType, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanel25 = new wxPanel(WxPanel23, ID_WXPANEL25, wxPoint(142, 5), wxDefaultSize/*wxSize(251, 49)*/);
	WxBoxSizer23->Add(WxPanel25, 2, wxALIGN_LEFT | wxALL |wxEXPAND, 5);

	WxBoxSizer25 = new wxBoxSizer(wxVERTICAL);
	WxPanel25->SetSizer(WxBoxSizer25);
	WxPanel25->SetAutoLayout(true);

	WxStaticText16 = new wxStaticText(WxPanel25, ID_WXSTATICTEXT16, _("Permission URL"), wxPoint(67, 5), wxDefaultSize, 0, _("WxStaticText16"));
	WxStaticText16->SetToolTip(_("(optional for online installers) A link to developer permission to bundle/release this app (eg. forum post or other online source)"));
	WxBoxSizer25->Add(WxStaticText16, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	WxEditPermissionLink = new wxTextCtrl(WxPanel25, ID_WXEDITPERMISSIONLINK, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(219, 26)*/, wxEXPAND, wxDefaultValidator, _("WxEditPermissionLink"));
	WxBoxSizer25->Add(WxEditPermissionLink, 2, wxALIGN_LEFT | wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM | wxEXPAND, 5);

//	WxPanel26 = new wxPanel(WxPanel23, ID_WXPANEL26, wxPoint(142, 5), wxDefaultSize/*wxSize(251, 49)*/);
	WxPanel26 = new wxPanel(WxPanel1, ID_WXPANEL26, wxPoint(142, 5), wxDefaultSize/*wxSize(251, 49)*/);
	WxBoxSizer1->Add(WxPanel26, 3, wxALIGN_LEFT | wxALL |wxEXPAND, 5);

	WxBoxSizer26 = new wxBoxSizer(wxVERTICAL);
	WxPanel26->SetSizer(WxBoxSizer26);
	WxPanel26->SetAutoLayout(true);

	WxStaticText18 = new wxStaticText(WxPanel26, ID_WXSTATICTEXT18, _("Permission Text"), wxPoint(67, 5), wxDefaultSize, 0, _("WxStaticText18"));
	WxStaticText18->SetToolTip(_("(optional for online installers) Developer permission to bundle/release this app (eg. from an email)"));
	WxBoxSizer26->Add(WxStaticText18, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	WxEditPermissionText = new wxTextCtrl(WxPanel26, ID_WXEDITPERMISSIONTEXT, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(219, 26)*/, wxTE_MULTILINE | wxEXPAND, wxDefaultValidator, _("WxEditPermissionText"));
	WxBoxSizer26->Add(WxEditPermissionText, 1, wxALIGN_LEFT | wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM | wxEXPAND, 5);

	WxBoxSizer6->Show(WxPanel23, false, true);
	WxBoxSizer1->Show(WxPanel26, false, true);
//	WxBoxSizer6->AddSpacer(2);


	WxPanel17 = new wxPanel(WxPanel1, ID_WXPANEL17, wxPoint(6, 277), wxDefaultSize/*wxSize(438, 69)*/);
	WxBoxSizer1->Add(WxPanel17, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 0);

	WxBoxSizer17 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel17->SetSizer(WxBoxSizer17);
	WxPanel17->SetAutoLayout(true);

	WxPanel18 = new wxPanel(WxPanel17, ID_WXPANEL18, wxPoint(5, 5), wxDefaultSize/*wxSize(248, 69)*/);
	WxBoxSizer17->Add(WxPanel18, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 5);

	WxBoxSizer18 = new wxBoxSizer(wxVERTICAL);
	WxPanel18->SetSizer(WxBoxSizer18);
	WxPanel18->SetAutoLayout(true);

	WxStaticText12 = new wxStaticText(WxPanel18, ID_WXSTATICTEXT12, _("MD5"), wxPoint(106, 5), wxDefaultSize, wxALIGN_LEFT, _("WxStaticText12"));
	WxStaticText12->SetToolTip(_("The MD5 hash of the installer"));
	WxBoxSizer18->Add(WxStaticText12, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	// Filter MD5 value to hexadecimal characters only.
	wxArrayString hexIncludeList;
	hexIncludeList.Add(wxT("0"));
	hexIncludeList.Add(wxT("1"));
	hexIncludeList.Add(wxT("2"));
	hexIncludeList.Add(wxT("3"));
	hexIncludeList.Add(wxT("4"));
	hexIncludeList.Add(wxT("5"));
	hexIncludeList.Add(wxT("6"));
	hexIncludeList.Add(wxT("7"));
	hexIncludeList.Add(wxT("8"));
	hexIncludeList.Add(wxT("9"));
	hexIncludeList.Add(wxT("a"));
	hexIncludeList.Add(wxT("b"));
	hexIncludeList.Add(wxT("c"));
	hexIncludeList.Add(wxT("d"));
	hexIncludeList.Add(wxT("e"));
	hexIncludeList.Add(wxT("f"));
	hexIncludeList.Add(wxT("A"));
	hexIncludeList.Add(wxT("B"));
	hexIncludeList.Add(wxT("C"));
	hexIncludeList.Add(wxT("D"));
	hexIncludeList.Add(wxT("E"));
	hexIncludeList.Add(wxT("F"));
	wxTextValidator md5Validator(wxFILTER_INCLUDE_CHAR_LIST);
	md5Validator.SetIncludes(hexIncludeList);

	WxEditMD5 = new wxTextCtrl(WxPanel18, ID_WXEDITMD5, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(231, 26)*/, wxEXPAND, md5Validator, _("WxEditMD5"));

//	WxEditMD5->DragAcceptFiles(true);
//	WxEditMD5->SetDropTarget(new DropFiles(WxEditMD5));

	WxBoxSizer18->Add(WxEditMD5, 1, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM | wxEXPAND, 5);

	WxPanel13 = new wxPanel(WxPanel1, ID_WXPANEL13, wxPoint(24, 351), wxDefaultSize/*wxSize(401, 127)*/);
	WxBoxSizer1->Add(WxPanel13, 4, wxALIGN_LEFT | wxALL | wxEXPAND, 5);

	WxBoxSizer13 = new wxBoxSizer(wxVERTICAL);
	WxPanel13->SetSizer(WxBoxSizer13);
	WxPanel13->SetAutoLayout(true);

	WxStaticText9 = new wxStaticText(WxPanel13, ID_WXSTATICTEXT13, _("Description"), wxPoint(166, 5), wxDefaultSize, 0, _("WxStaticText9"));
	WxStaticText9->SetToolTip(_("The description for the package.\nHTML tags allowed:  <a> <em> <strong> <cite> <code> <ul> <ol> <li> <dl> <dt> <dd> <pre> <s> <blockquote>"));
	WxBoxSizer13->Add(WxStaticText9, 0, wxALIGN_LEFT | wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 5);

	WxEditDescription = new wxTextCtrl(WxPanel13, ID_WXEDITDESCRIPTION, _(""), wxPoint(5, 24), wxDefaultSize/*wxSize(387, 95)*/, wxTE_MULTILINE | wxEXPAND, wxDefaultValidator, _("WxEditDescription"));
	WxBoxSizer13->Add(WxEditDescription, 1, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM | wxEXPAND, 5);

	WxPanel19 = new wxPanel(WxPanel1, ID_WXPANEL19, wxPoint(6, 488), wxDefaultSize);
	WxBoxSizer1->Add(WxPanel19, 0, wxALIGN_RIGHT | wxEXPAND | wxALL, 5);

	WxBoxSizer19 = new wxBoxSizer(wxHORIZONTAL);
	WxPanel19->SetSizer(WxBoxSizer19);
	WxPanel19->SetAutoLayout(true);

//WxPanel19->SetBackgroundColour(wxColour(0, 255, 0, 255));
	WxCheckBoxReleaseNotes = new wxCheckBox(WxPanel19, ID_WXCHECKBOXRELEASENOTES, _("Add Release Notes section"), wxDefaultPosition, wxDefaultSize);
	WxBoxSizer19->Add(WxCheckBoxReleaseNotes, 0, wxALIGN_LEFT | wxLEFT, 5);
//	WxButtonBuildThread = new wxButton(WxPanel19, ID_WXBUTTONBUILDTHREAD, _("Build"), wxPoint(5, 5), wxSize(86, 29), 0, wxDefaultValidator, _("WxButtonBuildThread"));
//	WxBoxSizer19->Add(WxButtonBuildThread, 0, wxALIGN_RIGHT | wxALL, 5);

	/*	Center line	*/
	WxPanelCenter = new wxPanel(WxPanelBase, ID_WXPANELCENTER, wxPoint(500, 0), wxDefaultSize/*wxSize(400, 547)*/, wxEXPAND);
	WxBoxSizerBase->Add(WxPanelCenter, 0, wxALIGN_LEFT | wxALL | wxEXPAND, 10);

	WxBoxSizerCenter = new wxBoxSizer(wxVERTICAL);
	WxPanelCenter->SetSizer(WxBoxSizerCenter);
	WxPanelCenter->SetAutoLayout(true);

	wxStaticLine* line = new wxStaticLine(WxPanelCenter, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL);
	WxBoxSizerCenter->Add(line, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 0);

	/*	Right hand content	*/
	WxPanelRight = new wxPanel(WxPanelBase, ID_WXPANELRIGHT, wxPoint(500, 0), wxDefaultSize/*wxSize(400, 547)*/, wxEXPAND);
	WxBoxSizerBase->Add(WxPanelRight, 10, wxALIGN_LEFT | wxALL | wxEXPAND, 10);

	WxBoxSizerRight = new wxBoxSizer(wxVERTICAL);
	WxPanelRight->SetSizer(WxBoxSizerRight);
	WxPanelRight->SetAutoLayout(true);

	WxPanelR1 = new wxPanel(WxPanelRight, ID_WXPANELR1, wxPoint(0, 0), wxDefaultSize);
	WxBoxSizerRight->Add(WxPanelR1, 0, wxALIGN_LEFT | wxALL | wxEXPAND, 5);

	WxBoxSizerR1 = new wxBoxSizer(wxVERTICAL);
	WxPanelR1->SetSizer(WxBoxSizerR1);
	WxPanelR1->SetAutoLayout(true);

	WxStaticTextR1 = new wxStaticText(WxPanelR1, ID_WXSTATICTEXTR1, _("Thread Title"), wxPoint(28, 5), wxDefaultSize, 0, _("WxStaticTextR1"));
	WxBoxSizerR1->Add(WxStaticTextR1, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP | wxEXPAND, 5);

	WxHtmlOutputTitle = new wxHtmlWindow(WxPanelR1, ID_WXHTMLOUTPUTTITLE, wxPoint(5, 24), wxSize(231, 34), wxHW_SCROLLBAR_NEVER);
	WxBoxSizerR1->Add(WxHtmlOutputTitle, 1, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxBOTTOM | wxEXPAND, 5);

	WxPanelR3 = new wxPanel(WxPanelR1, ID_WXPANELR3, wxPoint(0, 0), wxDefaultSize, wxEXPAND);
	WxBoxSizerR1->Add(WxPanelR3, 0, wxALIGN_RIGHT | wxRIGHT | wxEXPAND, 5);

	WxBoxSizerR3 = new wxBoxSizer(wxHORIZONTAL);
	WxPanelR3->SetSizer(WxBoxSizerR3);
	WxPanelR3->SetAutoLayout(true);

	WxBoxSizerR3->AddStretchSpacer();

	WxButtonCopyTitle = new wxButton(WxPanelR3, ID_WXBUTTONCOPYTITLE, _("Copy To Clipboard"), wxDefaultPosition, wxDefaultSize);
	WxBoxSizerR3->Add(WxButtonCopyTitle, 0, wxALIGN_RIGHT | wxRIGHT, 5);
	WxButtonCopyTitle->Disable();

	WxPanelR2 = new wxPanel(WxPanelRight, ID_WXPANELR2, wxPoint(5, 5), wxDefaultSize, wxEXPAND);
	WxBoxSizerRight->Add(WxPanelR2, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 5);

	WxBoxSizerR2 = new wxBoxSizer(wxVERTICAL);
	WxPanelR2->SetSizer(WxBoxSizerR2);
	WxPanelR2->SetAutoLayout(true);

	WxStaticTextR2 = new wxStaticText(WxPanelR2, ID_WXSTATICTEXTR2, _("Thread Body"), wxPoint(28, 5), wxDefaultSize, 0, _("WxStaticTextR2"));
	WxBoxSizerR2->Add(WxStaticTextR2, 0, wxALIGN_LEFT | wxLEFT | wxRIGHT | wxTOP, 5);

	WxHtmlOutputBody = new wxHtmlWindow(WxPanelR2, ID_WXHTMLOUTPUTBODY, wxPoint(5, 24), wxSize(400, 400), wxHW_SCROLLBAR_NEVER | wxEXPAND);
	WxBoxSizerR2->Add(WxHtmlOutputBody, 1, wxALIGN_LEFT | wxALL | wxEXPAND, 5);
	WxHtmlOutputBody->SetHTMLBackgroundColour(wxColour(255, 255, 255, wxALPHA_OPAQUE));
//	WxBoxSizerR1->Add(WxHtmlOutputTitle, 0, wxALIGN_LEFT | wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 5);

	WxPanelR4 = new wxPanel(WxPanelR2, ID_WXPANELR4, wxPoint(0, 0), wxDefaultSize, wxEXPAND);
	WxBoxSizerR2->Add(WxPanelR4, 0, wxALIGN_RIGHT | wxRIGHT | wxEXPAND, 5);

	WxBoxSizerR4 = new wxBoxSizer(wxHORIZONTAL);
	WxPanelR4->SetSizer(WxBoxSizerR4);
	WxPanelR4->SetAutoLayout(true);

	WxBoxSizerR4->AddStretchSpacer();

	WxButtonCopyBody = new wxButton(WxPanelR4, ID_WXBUTTONCOPYBODY, _("Copy To Clipboard"), wxDefaultPosition, wxDefaultSize);
	WxBoxSizerR4->Add(WxButtonCopyBody, 0, wxALIGN_RIGHT | wxRIGHT, 5);
	WxButtonCopyBody->Disable();

	WxMenuBar1 = new wxMenuBar();
	wxMenu *ID_MNU_FILE_1101_Mnu_Obj = new wxMenu();
	ID_MNU_FILE_1101_Mnu_Obj->Append(ID_MNU_EXIT_1102, _("E&xit\tAlt-F4"), _(""), wxITEM_NORMAL);
	WxMenuBar1->Append(ID_MNU_FILE_1101_Mnu_Obj, _("&File"));

	wxMenu *ID_MNU_HELP_1103_Mnu_Obj = new wxMenu();
	ID_MNU_HELP_1103_Mnu_Obj->Append(ID_MNU_UPDATE_1106, _("&Check for Updates"), _(""), wxITEM_NORMAL);
	ID_MNU_HELP_1103_Mnu_Obj->AppendSeparator();
	ID_MNU_HELP_1103_Mnu_Obj->Append(ID_MNU_WEBSITE_1104, _("&Website"), _(""), wxITEM_NORMAL);
	ID_MNU_HELP_1103_Mnu_Obj->Append(ID_MNU_ABOUT_1105, _("&About"), _(""), wxITEM_NORMAL);
	WxMenuBar1->Append(ID_MNU_HELP_1103_Mnu_Obj, _("&Help"));
	SetMenuBar(WxMenuBar1);

	SetTitle(_("Development Test Thread Builder"));
	SetIcon(wxNullIcon);

	Center();

	////GUI Items Creation End
}

void DevTestThreadBuilder::OnClose(wxCloseEvent& event)
{
	Destroy();
}

/*
 * Mnuexit1102Click
 */
void DevTestThreadBuilder::Mnuexit1102Click(wxCommandEvent& event)
{
	Close(true);
}

/*
 * Mnuwebsite1104Click
 */
void DevTestThreadBuilder::Mnuwebsite1104Click(wxCommandEvent& event)
{
	wxLaunchDefaultBrowser(wxT("http://firedancer-software.com/software/devtestthreadbuilder"));
}

/*
 * Mnuabout1105Click
 */
void DevTestThreadBuilder::Mnuabout1105Click(wxCommandEvent& event)
{
	wxAboutDialogInfo info;
	info.SetName(PRODUCT_NAME);
    info.SetVersion(PRODUCT_VERSION);
    info.SetCopyright(_("(C) 2013 Firedancer Software.\nLicenced under the GPL Version 3.\n"));
    info.SetDescription(wxT("Builds a preformatted forum topic for PortableApps development test releases."));

	wxAboutBox(info);
}

/*
 * Mnuupdate1106Click
 */
void DevTestThreadBuilder::Mnuupdate1106Click(wxCommandEvent& event)
{
	CheckForUpdates(false);
}



/*
 * WxChoice1Selected
 */
void DevTestThreadBuilder::WxChoice1Selected(wxCommandEvent& event)
{
	wxString selectedItem = WxChoiceLicence->GetStringSelection();

	if(selectedItem == "Open Source")
	{
		WxBoxSizer6->Show(WxPanel8, true, true);
		WxBoxSizer6->Show(WxPanel23, false, true);
		WxBoxSizer1->Show(WxPanel26, false, true);
	}
	else
	{
		WxBoxSizer6->Show(WxPanel8, false, true);
		WxBoxSizer6->Show(WxPanel23, true, true);

		wxString permType = WxChoicePermissionType->GetStringSelection();

		if(permType == "Link")
		{
			WxBoxSizer23->Show(WxPanel25, true, true);
			WxBoxSizer1->Show(WxPanel26, false, true);
		}
		else
		{
			WxBoxSizer23->Show(WxPanel25, false, true);
			WxBoxSizer1->Show(WxPanel26, true, true);
		}
	}
	WxBoxSizer6->Layout();
	WxBoxSizer1->Layout();
	UpdateOutput();
}


/*
 * WxChoicePermissionType
 */
void DevTestThreadBuilder::WxChoicePermissionTypeChanged(wxCommandEvent& event)
{
	wxString selectedItem = WxChoicePermissionType->GetStringSelection();

	if(selectedItem == "Link")
	{
		WxBoxSizer23->Show(WxPanel25, true, true);
		WxBoxSizer1->Show(WxPanel26, false, true);
	}
	else
	{
		WxBoxSizer23->Show(WxPanel25, false, true);
		WxBoxSizer1->Show(WxPanel26, true, true);
	}
	WxBoxSizer23->Layout();
	WxBoxSizer1->Layout();
	UpdateOutput();
}


/*
 * WxChoiceInstallTypeChanged
 */
void DevTestThreadBuilder::WxChoiceInstallTypeChanged(wxCommandEvent& event)
{
	wxString selectedItem = WxChoiceInstallType->GetStringSelection();

	if(selectedItem == "Online")
	{
		WxPanel22->Show();
	}
	else
	{
		WxPanel22->Hide();
	}
	UpdateOutput();
}

void DevTestThreadBuilder::UpdateOutput()
{
	wxColour errorColour(255, 220, 220, wxALPHA_OPAQUE);
	wxColour standardColour(255, 255, 255, wxALPHA_OPAQUE);
	bool isTitleValid = true;
	bool isBodyValid = true;

	wxString appName = WxEditAppName->GetLineText(0);

	if(appName == "")
	{
		WxEditAppName->SetBackgroundColour(errorColour);
		isTitleValid = false;
		isBodyValid = false;
		appName = "????";
	}
	else
	{
		WxEditAppName->SetBackgroundColour(standardColour);
	}
	WxEditAppName->Refresh();

	wxString homepage = WxEditHomepage->GetLineText(0);

	if(homepage == "")
	{
		WxEditHomepage->SetBackgroundColour(errorColour);
		isBodyValid = false;
		homepage = "????";
	}
	else
	{
		WxEditHomepage->SetBackgroundColour(standardColour);
	}
	WxEditHomepage->Refresh();

	wxString appVersion = WxEditAppVersion->GetLineText(0);

	if(appVersion == "")
	{
		WxEditAppVersion->SetBackgroundColour(errorColour);
		isTitleValid = false;
		isBodyValid = false;
		appVersion = "????";
	}
	else
	{
		WxEditAppVersion->SetBackgroundColour(standardColour);
	}
	WxEditAppVersion->Refresh();

	int devTestVersion = WxSpinCtrl1->GetValue();

	wxString appCategory = WxChoiceCategory->GetStringSelection();

	wxString downloadLink = WxEditDownloadLink->GetLineText(0);

	if(downloadLink == "")
	{
		WxEditDownloadLink->SetBackgroundColour(errorColour);
		isBodyValid = false;
		downloadLink = "????";
	}
	else
	{
		WxEditDownloadLink->SetBackgroundColour(standardColour);
	}
	WxEditDownloadLink->Refresh();

	wxString downloadSize = WxEditDownloadSize->GetLineText(0);
//	wxString downloadSize = wxString::Format(wxT("%f"), fDownloadSize);
	if(downloadSize == "")
	{
		WxEditDownloadSize->SetBackgroundColour(errorColour);
		isBodyValid = false;
		downloadSize = "????";
	}
	else
	{
		WxEditDownloadSize->SetBackgroundColour(standardColour);
	}
	WxEditDownloadSize->Refresh();

	wxString installSize = WxEditInstallSize->GetLineText(0);

	if(installSize == "")
	{
		WxEditInstallSize->SetBackgroundColour(errorColour);
		isBodyValid = false;
		installSize = "????";
	}
	else
	{
		WxEditInstallSize->SetBackgroundColour(standardColour);
	}
	WxEditInstallSize->Refresh();


	wxString installType = WxChoiceInstallType->GetStringSelection();
	bool bIsOnlineInstaller = false;
	wxString additionalDownloadSize = "";

	if(installType == "Online")
	{
		bIsOnlineInstaller = true;
		additionalDownloadSize = WxEditAdditionalSize->GetValue();
	}

	wxString appLicence = WxChoiceLicence->GetStringSelection();

	bool bIsOpenSource = false;
	bool bIsFreewareLink = false;
	bool bIsFreewareText = false;
	wxString sourceLink = "";
	wxString permLink = "";
	wxString permText = "";

	if(appLicence == "Open Source")
	{
		bIsOpenSource = true;
		sourceLink = WxEditSourceCodeLink->GetLineText(0);
	}
	else
	{
		wxString permType = WxChoicePermissionType->GetStringSelection();

		if(permType == "Link")
		{
			bIsFreewareLink = true;
			permLink = WxEditPermissionLink->GetLineText(0);

			if(!bIsOnlineInstaller && permLink == "")
			{
				WxEditPermissionLink->SetBackgroundColour(errorColour);
				isBodyValid = false;
			}
			else
			{
				WxEditPermissionLink->SetBackgroundColour(standardColour);
			}
			WxEditPermissionLink->Refresh();
		}
		else
		{
			bIsFreewareText = true;
			permText = WxEditPermissionText->GetValue();

			if(!bIsOnlineInstaller && permText == "")
			{
				WxEditPermissionText->SetBackgroundColour(errorColour);
				isBodyValid = false;
			}
			else
			{
				WxEditPermissionText->SetBackgroundColour(standardColour);
			}
			WxEditPermissionText->Refresh();
		}
	}

	wxString md5Hash = WxEditMD5->GetLineText(0);

	if(md5Hash == "" || md5Hash.Length() != 32)
	{
		WxEditMD5->SetBackgroundColour(errorColour);
		isBodyValid = false;
		md5Hash = "????";
	}
	else
	{
		WxEditMD5->SetBackgroundColour(standardColour);
	}
	WxEditMD5->Refresh();

	wxString description = WxEditDescription->GetValue();

	if(description == "")
	{
		WxEditDescription->SetBackgroundColour(errorColour);
		isBodyValid = false;
		description = "????";
	}
	else
	{
		WxEditDescription->SetBackgroundColour(standardColour);
	}
	WxEditDescription->Refresh();

	bool bShowReleaseNotes = WxCheckBoxReleaseNotes->IsChecked();


	outputTitle = "";

	outputTitle << appName << " Portable " << appVersion << " Development Test " << devTestVersion;

	WxHtmlOutputTitle->SetPage(outputTitle);



	outputBody = "";

	outputBody << "<strong>Application:</strong> ";
	if(homepage != "????")
	{
		outputBody << "<a href='" << homepage << "'>";
	}
	outputBody << appName;

	if(homepage != "????")
	{
		outputBody << "</a>";
	}

	outputBody << "\n";
	outputBody << "<strong>Category:</strong> " << appCategory << "\n";
	outputBody << "<strong>Licence:</strong> " << appLicence;

	if(bIsOpenSource && sourceLink != "")
	{
		outputBody << " (<a href='" << sourceLink << "'>source code</a>)";
	}
	if(bIsFreewareLink && permLink != "")
	{
		outputBody << " (<a href='" << permLink << "'>permission</a>)";
	}

	outputBody << "\n";
	outputBody << "<strong>Description:</strong> " << description << "\n\n";

	if(bIsFreewareText && permText != "")
	{
		outputBody << "\n";
		outputBody << "<strong>Permission:</strong> " << permText << "\n\n";
	}

	if(downloadLink != "????")
	{
		outputBody << "<a href='" << downloadLink << "'>";
	}

	outputBody << "Download " << appName << " Portable " << appVersion << " Development Test " << devTestVersion;

	if(downloadLink != "????")
	{
		outputBody << "</a>";
	}

	outputBody << " [" << downloadSize << "MB download / " << installSize << "MB installed]\n";
	outputBody << "(MD5: " << md5Hash << ")\n\n";

	if(bIsOnlineInstaller)
	{
		outputBody << "\n<strong>Online Installer:</strong> This app features an online installer which will download ";
		if(additionalDownloadSize.IsEmpty())
		{
			outputBody << "additional";
		}
		else
		{
			outputBody << "an additional " << additionalDownloadSize << "MB of";
		}
		outputBody << " data during installation.\n\n";
	}

	if(bShowReleaseNotes)
	{
		wxDateTime now = wxDateTime::Now();

		outputBody << "<strong>Release Notes</strong>:\n";
		outputBody << "<em>\n";
		outputBody << "Development Test " << devTestVersion << " (" << now.Format("%Y-%m-%d") << "):\n";
//		outputBody << "<ul>\n";
//		outputBody << "<li></li>\n";
//		outputBody << "</ul>\n";
		outputBody << "</em>\n";
	}

	while(outputBody.Find("\n\n\n") != wxNOT_FOUND)
	{
		outputBody.Replace("\n\n\n", "\n\n");
	}

	wxString htmlOutput = outputBody;
	htmlOutput.Replace("\n", "<br>");
//	htmlOutput.Replace("<a", "<a style=\"color:#820606\"");
//	htmlOutput.Replace("<a", "<font color=#820606><a");
//	htmlOutput.Replace("</a>", "</a></font>");

//	wxMessageBox(htmlOutput);
	WxHtmlOutputBody->SetPage(htmlOutput);

	if(isTitleValid)
	{
		WxButtonCopyTitle->Enable();
	}
	else
	{
		WxButtonCopyTitle->Disable();
	}

	if(isBodyValid)
	{
		WxButtonCopyBody->Enable();
	}
	else
	{
		WxButtonCopyBody->Disable();
	}
}


/*
 * WxButtonBuildThreadClick
 */
void DevTestThreadBuilder::OnCtrlUpdate(wxCommandEvent& event)
{
	UpdateOutput();
}


void DevTestThreadBuilder::OnSpinCtrlUpdate(wxSpinEvent& event)
{
	UpdateOutput();
}


void DevTestThreadBuilder::CheckForUpdates(bool bCheckSilently)
{
	bIsCheckingUpdates = true;
	WxMenuBar1->Enable(ID_MNU_UPDATE_1106, false);
    CheckForUpdatesThread *thread = new CheckForUpdatesThread(this, bCheckSilently);
    thread->Create();
    thread->Run();
}


void DevTestThreadBuilder::OnCheckUpdateThread(wxCommandEvent& event)
{
	WxMenuBar1->Enable(ID_MNU_UPDATE_1106, true);
	bIsCheckingUpdates = false;
	return;
}


void DevTestThreadBuilder::CopyToClipboard(wxString text)
{
	if (wxTheClipboard->Open())
	{
		wxTheClipboard->SetData(new wxTextDataObject(text));
		wxTheClipboard->Close();
	}
}

void DevTestThreadBuilder::OnCopyTitleClick(wxCommandEvent& event)
{
	CopyToClipboard(outputTitle);
}

void DevTestThreadBuilder::OnCopyBodyClick(wxCommandEvent& event)
{
	CopyToClipboard(outputBody);
}

