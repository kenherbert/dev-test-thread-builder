//---------------------------------------------------------------------------
//
// Name:        DevTestThreadBuilderApp.cpp
// Author:      Ken
// Created:     5/03/2013 9:48:36 AM
// Description:
//
//---------------------------------------------------------------------------

#include "DevTestThreadBuilderApp.h"
#include "DevTestThreadBuilder.h"
#include <wx/protocol/http.h>

IMPLEMENT_APP(DevTestThreadBuilderApp)

bool DevTestThreadBuilderApp::OnInit()
{
	wxHTTP::Initialize();
    DevTestThreadBuilder* frame = new DevTestThreadBuilder(NULL);
    SetTopWindow(frame);
	frame->SetIcon(wxICON(appicon));
	frame->Show();
    return true;
}

int DevTestThreadBuilderApp::OnExit()
{
	return 0;
}
