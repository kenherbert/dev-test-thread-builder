//---------------------------------------------------------------------------
//
// Name:        DevTestThreadBuilderApp.h
// Author:      Ken
// Created:     5/03/2013 9:48:36 AM
// Description:
//
//---------------------------------------------------------------------------

#ifndef __DEVTESTTHREADBUILDERApp_h__
#define __DEVTESTTHREADBUILDERApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class DevTestThreadBuilderApp : public wxApp
{
	public:
		bool OnInit();
		int OnExit();
};

#endif
